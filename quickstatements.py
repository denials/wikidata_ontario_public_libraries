#!/usr/bin/env python3

"""
Open data: https://www.ontario.ca/data/ontario-public-library-statistics

Goal is to either append statements to existing Wikidata items or create new
Wikidata items using the QuickStatements v2 syntax:
https://www.wikidata.org/wiki/Help:QuickStatements

The original data has been modified as follows:

* Library names have been updated based on the web site data
* Column 2 ('QID') holds the manually-found QID of the existing Wikidata item
* Column 3 ('Instance of') has been added to indicate the instance of value
  (library system or public library)
* 'Library Full Name' was reconciled using OpenRefine, and either contains the
  text for the stub of the library name, the full library name, or the QID
* 'A1.3 Ontario Library Service (OLS) Region (English)' was reconciled with
  OpenRefine to provide a QID
* 'Admin Territory' was cloned from 'A1.10 City/Town' then reconciled to provide
  a QID
"""

import csv
import re

def gen_id(qs, row, lib):
    "LAST unless we have a QID"

    if row['QID']:
        lib['QID'] = row['QID']
    elif is_qid(row['Library Full Name']):
        lib['QID'] = row['Library Full Name']
    else:
        qs.write("CREATE\n")
        lib['QID'] = 'LAST'

def is_qid(value):
    "Is the value a QID?"

    if re.search('^Q\d\d', value) is not None:
        return True
    return False
    

def label(qs, row, lib):
    "Generate a label if necessary"

    if row['QID'] or is_qid(lib['QID']):
        return
    desc = ''
    label = row['Library Full Name'] 
    if row['Instance of'] == 'Q28324850':
        desc = 'library system in Ontario, Canada'
        if label.find('Library') == -1:
            label = label + ' Library System'
    else:
        desc = 'public library in Ontario, Canada'
        if label.find('Library') == -1:
            label = label + ' Public Library'
    qs.write('%s\tLen\t"%s"\n' % (lib['QID'], label))
    qs.write('%s\tDen\t"%s"\n' % (lib['QID'], desc))

def country(qs, row, lib):
    "We're all about Canada"

    qs.write("%s\tP17\t%s" % (lib['QID'], 'Q16'))
    append_source(qs)

def instance(qs, row, lib):
    """
    public library or library system

    always include public library
    """

    public_library = 'Q28564'

    instance = row['Instance of']
    if instance != '' and instance != public_library:
        qs.write("%s\tP31\t%s" % (lib['QID'], instance))
        append_source(qs)
    # library systems also have a public library
    qs.write("%s\tP31\t%s" % (lib['QID'], public_library))
    append_source(qs)

def member_of(qs, row, lib):
    """
    Part of a major library system?
    """

    member = row['A1.3 Ontario Library Service (OLS) Region (English)']
    if member is None:
        return
    qs.write("%s\tP463\t%s" % (lib['QID'], member))
    append_source(qs)

def admin_territory(qs, row, lib):
    """
    Located in administrative territory

    Default to Ontario
    """
    
    terr_id = 'Q1904'
    if is_qid(row['Admin Territory']) is not None:
        terr_id = row['Admin Territory']
    qs.write("%s\tP131\t%s" % (lib['QID'], terr_id))
    append_source(qs)

def address(qs, row, lib):
    """
    Physical street address
    """

    street = row['A1.9 Street Address'].strip()
    if street == '':
        street = row['A1.5 Mailing Address'].strip()
    city = row['A1.10 City/Town'].strip()
    province = 'Ontario'.strip()
    post_code = row['A1.12 Postal Code'][0:3] + ' ' + row['A1.12 Postal Code'][3:].strip()

    qs.write('%s\tP969\t"%s, %s, %s, %s"' % (lib['QID'], street, city, province, post_code))
    append_source(qs)

def url(qs, row, lib):
    """
    Official website
    """

    url = row['A1.13 Web Site Address'].strip()
    if url == '':
        return
    elif not url.startswith('http'):
        url = 'http://' + url

    qs.write('%s\tP856\t"%s"' % (lib['QID'], url))
    append_source(qs)

def visitors(qs, row, lib):
    "Annual visitors, qualified by year"

    year = '2016'
    visits = row['G1.5.1.W  No. of visits to the library made in person']
    qs.write("%s\tP1174\t%s\tP585\t+%s-00-00T00:00:00Z/9" % (lib['QID'], visits, year))
    append_source(qs)

def library_id(qs, row, lib):
    "Ontario public library ID"

    year = '2016'
    opl_id = row['Library Number']
    qs.write("%s\tP5122\t\"%s\"" % (lib['QID'], opl_id))
    append_source(qs)


def append_source(qs):
    "All of our statements come from Ontario open data"

    qs.write('\tS248\t%s\tS854\t"%s"\tS813\t%s\n' % ("Q52147771", "https://files.ontario.ca/opendata/ontario_public_library_statistics_open_data_2016.csv", "+2018-04-22T00:00:00Z/11"))
    

if __name__ == '__main__':

    with open('quickstatements.csv', 'w') as qs:

        with open('refined.csv', newline='') as csvfile:
            libreader = csv.DictReader(csvfile)
            for row in libreader:
                if row['A1.4 Type of Library Service (English)'].find('Contracting') > -1:
                    continue
                lib = {}
                gen_id(qs, row, lib)
                label(qs, row, lib)
                instance(qs, row, lib)
                country(qs, row, lib)
                admin_territory(qs, row, lib)
                member_of(qs, row, lib)
                address(qs, row, lib)
                url(qs, row, lib)
                visitors(qs, row, lib)
                library_id(qs, row, lib)
                    
            print(libreader.fieldnames)
