#!/usr/bin/env python3

import csv
import googlemaps
import json
import sys
from datetime import datetime

"""
Get coordinates for street addresses via the Google Geocoding API

1. Generate a list of Ontario public libraries without coordinates and download as TSV:

    SELECT ?item ?address WHERE {
      ?item wdt:P5122 ?opl_id .
      ?item wdt:P969 ?address
      OPTIONAL { ?item wdt:P625 ?coords }
      FILTER (!bound(?coords))
    }
    ORDER BY ?item

2. Run the street addresses through the Google Maps Geocoder and write out the data
   in QuickStatements format so we can import it into Wikidata

3. Load the statements via QuickStatements: https://tools.wmflabs.org/quickstatements/

4. Generate a cool map showing the fruit of your labours:

    #defaultView:Map
    SELECT ?item ?itemLabel ?coords WHERE {
      ?item wdt:P5122 ?opl_id .
      OPTIONAL { ?item wdt:P625 ?coords }
      SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    }
    ORDER BY ?coords
"""

# Get a Geocoding API key from the Google Developer Console
api_key = ''
gmaps = googlemaps.Client(key=api_key)

# Ontario public libraries without coordinates but with addresses
source = 'opl_addresses.tsv'

# QuickStatements file that will hold the QID and the coordinates
dest = 'opl_coords_quickstatements.tsv'


with open(dest, 'w') as qs:

    with open(source, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            print(row)
            address = row['address']

            # Geocode an address
            result = gmaps.geocode(address)

            lat = result[0]['geometry']['location']['lat']
            lng = result[0]['geometry']['location']['lng']
            qs.write("%s\tP625\t@%s/%s" % (row['item'], lat, lng))
